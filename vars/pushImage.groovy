def call(String imageName) {
    echo ("Pushing the image to dockerhub...")
    withCredentials([usernamePassword(credentialsId:'jenkins-dockerhub',usernameVariable:'user',passwordVariable:'password')]) {
        sh "echo ${password} | docker login -u ${user} --password-stdin"
        sh "docker push ${imageName} "
    }
}
